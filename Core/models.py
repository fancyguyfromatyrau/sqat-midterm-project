from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils import timezone


class Core(models.Model):
    title = models.CharField(max_length=255, null=False)
    description = models.CharField(max_length=255, null=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='core')
    slug = models.SlugField(max_length=32, unique=True)
    updated = models.DateTimeField(auto_now=True)
    published = models.DateTimeField(default=timezone.now)

    def get_absolute_url(self):
        return reverse('core:single', args=[self.slug])

    class Meta:
        db_table = 'notes'
        ordering = ['-published']
        verbose_name = 'Core'
        verbose_name_plural = 'Cores'

    def __str__(self):
        return self.title



