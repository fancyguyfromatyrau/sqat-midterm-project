from django.contrib.auth.mixins import UserPassesTestMixin
from django.test import TestCase
from .models import Core, User


class CoreModelTestCase(TestCase):
    def test_title_label(self):
        """Tests if a note has title filed"""
        admin = User.objects.create(username='mussa', password='mussa')
        admin.save()
        note = Core.objects.create(title="My first post", author_id=admin.id)
        note.save()
        self.assertEqual(note.title, 'My first post')

    def test_description_label(self):
        """Tests if a note has title filed"""
        admin = User.objects.create(username='mussa', password='mussa')
        admin.save()
        note = Core.objects.create(description="My first post description", author_id=admin.id)
        note.save()
        self.assertEqual(note.description, 'My first post description')

    def test_slug_label(self):
        """Tests if a note has title filed"""
        admin = User.objects.create(username='mussa', password='mussa')
        admin.save()
        note = Core.objects.create(slug="core-test", author_id=admin.id)
        note.save()
        self.assertEqual(note.slug, 'core-test')

    def test_title_and_description_max_length(self):
        admin = User.objects.create(username='mussa', password='mussa')
        admin.save()
        author = Core.objects.create(
            title='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ac mollis tellus. Sed porta libero '
                  'leo, interdum eleifend nulla rhoncus sit amet.',
            description='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ac mollis tellus. Sed porta '
                        'libero '
                  'leo, interdum eleifend nulla rhoncus sit amet.',
            author_id=admin.id)
        max_length_title = author._meta.get_field('title').max_length
        max_length_description = author._meta.get_field('description').max_length
        self.assertGreater(max_length_title, 100)
        self.assertGreater(max_length_description, 100)


class UserModelTestCase(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create_user('Chevy Chase', 'chevy@chase.com', 'chevyspassword')
        self.user_2 = User.objects.create_user('Jim Carrey', 'jim@carrey.com', 'jimspassword')
        self.user_3 = User.objects.create_user('Dennis Leary', 'dennis@leary.com', 'denisspassword')

    def test_for_user_count(self):
        array_of_created_users = len([self.user_1, self.user_2, self.user_3])
        number_of_users_in_db = User.objects.count()
        self.assertGreaterEqual(number_of_users_in_db, array_of_created_users)

    def tearDown(self):
        # Clean up after each test
        self.user_1.delete()
        self.user_2.delete()
        self.user_3.delete()


class IndexViewTest(TestCase):
    template_name = 'core/index.html'

    def test_template_name(self):
        self.client.login()
        response = self.client.get('')
        self.assertTemplateUsed(response, 'core/index.html')

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)


def test_core_creation(self):
    admin = User.objects.create(username='mussa', password='mussa')
    admin.save()
    note1 = Core.objects.create(title="NNote1", author_id=admin.id, description="Description of NNote1 post")
    note1.save()
    self.assertTrue(isinstance(note1, Core))
    self.assertEqual(note1.__str__(), note1.title)