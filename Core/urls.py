from django.urls import path
from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('/posts', views.PostsView.as_view(), name='posts'),
    path('/add', views.AddView.as_view(), name='add'),
    path('/edit/<int:pk>', views.EditView.as_view(), name='edit'),
    path('delete/<int:pk>', views.Delete.as_view(), name='delete'),
    path('<slug:slug>/', views.SingleView.as_view(), name='single'),
]