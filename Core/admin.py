from django.contrib import admin
from .models import Core, User


@admin.register(Core)
class PostAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',), }

# @admin.register(User)
# class PersonAdmin(admin.ModelAdmin):
#     pass
